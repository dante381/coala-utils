.. coala documentation master file, created by
   sphinx-quickstart on Fri Mar  10 15:35:01 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. meta::
   :description: coala is a simple COde AnaLysis Application. Its goal is
                 to make static code analysis easy while remaining completely
                 modular and therefore extendable and language independent.
   :keywords:    coala, code analysis, static code analysis, linter,
                 language agnostic, python3, linux, unix, windows, bears,
                 coala-bears, antlr4, parse trees

Welcome to coala-utils API Documentation
========================================

.. image:: _static/images/coala-header.png
   :scale: 50
   :align: center

Hey there! You're in the right place if you:

- want to use coala-utils

If you're trying to **use** coala, you should have a look at our
`user documentation`_ instead.


.. toctree::
   :caption: Home
   :hidden:

   Welcome <self>
   Homepage <http://coala.io/#/home>
   Code of Conduct <http://coala.io/coc>

.. toctree::
   :caption: coala-utils API Documentation
   :maxdepth: 4

   coala_utils

.. _user documentation: https://docs.coala.io
