import os
import unittest

from coala_utils.FileUtils import detect_encoding


class FileUtilsTest(unittest.TestCase):

    def test_detect_encoding(self):
        current_dir = os.getcwd()
        file = os.path.join(current_dir, 'file')
        content = 'Hello World!\n'
        with open(file, 'w', encoding='utf-8') as fd:
            fd.write(content)
            fd.close()
        result1 = detect_encoding(file)
        with open(file, 'w', encoding='utf-16') as fd:
            fd.truncate(0)
            fd.write(content)
            fd.close()
        result2 = detect_encoding(file)
        with open(file, 'w', encoding='utf-32') as fd:
            fd.truncate(0)
            fd.write(content)
        result3 = detect_encoding(file)
        self.assertEqual('utf-8', result1)
        self.assertEqual('utf-16', result2)
        self.assertEqual('utf-32', result3)
        os.remove(file)
